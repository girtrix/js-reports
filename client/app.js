
var url = "http://localhost:5488";
var repData = { "to": "Pavel Sladek", "from": "Jan Blaha",  "price": 800 };

var request = {
	template: { 
		name: 'invoice-template', engine: 'handlebars', recipe: 'phantom-pdf'
	},
	data: repData
};

function callrep() {
	let postData =  { 
      "template": { "name" : "invoice-template" },
      "data" : repData
   	};

	$.post(url+'/api/report', postData)
		.done(function( result ) {
			console.log('OK');
		})
		.fail(function( err ) {
			alert( "Errore: " + err );
		});
}

function callrep2() {
	jsreport.serverUrl = url;

	jsreport.renderAsync(request).then(function(res) {
		$('#res').prop('src', res.toDataURI());
	});

	//display report in the new tab
	//jsreport.render('_blank', request);

	//display report in placeholder with id reportPlaceholder
	//jsreport.render('foo', request);

	//display report in placeholder element
	//jsreport.render(document.getElementById('reportPlaceholder'), request);
}

function callrep3() {
	jsreport.serverUrl = 'http://localhost:5488';
	var repData = { "to": "Pavel Sladek", "from": "Jan Blaha",  "price": 800 };

	var request = {
	  template: { 
	    name: 'invoice-template', engine: 'handlebars', recipe: 'phantom-pdf'
	   },
	   data: repData
	};

	jsreport.renderAsync(request).then(function(res) {                        
	    var html = '<html>' +
	            '<style>html,body {padding:0;margin:0;} iframe {width:100%;height:100%;border:0}</style>' +
	            '<body>' +                                
	            '<iframe type="application/pdf" src="' +  res.toDataURI() + '"></iframe>' +
	            '</body></html>';
	    var mywin = window.open("about:blank", "Report");
	    mywin.document.write(html);
	    mywin.document.close();
	});
}

function scaricaPdf() {
	jsreport.serverUrl = url;

	//open download dialog for report
	jsreport.download('myReport.pdf', request);
}
